
INTRODUCTION
------------
This module enables teams to track outstanding cases which need resolution.
As it is dependent on CCK (i.e. content module), it allows customizing your CCKase tracker
options, Views, Token, rules modules integration, etc'.


INSTALLATION
------------
1. Copy the files to your sites/all/modules directory.
2. Enable the CCKase tracker module and its dependencies at admin/modules.
3. In admin/settings/cckasetracker set at least one content type as a
   'CCKase tracker node'.
4. Enable the fields that will act as 'CCKase tracker fields'.
5. Create a case content - make sure comments are enabled. Whenever a change in
   the CCKase tracker fields will be made, you will see the diff in the top of
   that comment.

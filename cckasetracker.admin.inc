<?php

/**
 * @file
 * Admin page callbacks for the cckasetracker module.
 */

/**
 * Menu callback; Configures the various CCKase tracker general options.
 */
function cckasetracker_admin_settings() {
  $form = array();

  $cckasetracker_types = cckasetracker_get_types();
  if (!in_array('case', $cckasetracker_types)) {
    form_set_error('content_types_table', t('You must designate at least one content type to act as a project and another as a case. <a href="!create">Create new content type</a> if needed.', array('!create' => url('admin/content/types/add', array('query' => drupal_get_destination())))));
  }
  $form['admin_cckasetracker_content_types'] = array(
    '#type' => 'fieldset',
    '#title' => t('Content types'),
    '#collapsible' => TRUE,
    '#collapsed' => $cckasetracker_types,
  );

  $header = array(t('Type'), t('Usage'), t('Operations'));
  $rows = array();
  $types_map = cckasetracker_types_map();
  foreach (node_get_types() as $type_name => $value) {
    $usage = !empty($cckasetracker_types[$type_name]) ? $cckasetracker_types[$type_name] : 'omitted';

    $type_url_str = str_replace('_', '-', $type_name);
    $rows[] = array(
      check_plain($type_name),
      $types_map[$usage],
      l(t('Edit'), "admin/content/node-type/$type_url_str", array('query' => drupal_get_destination())),
    );
  }
  $form['admin_cckasetracker_content_types']['content_types_table'] = array('#value' => theme('table', $header, $rows));

  // Per content type settings.
  foreach ($cckasetracker_types as $type_name => $value) {
    $type_name = drupal_strtolower($type_name);
    $form['cckasetracker_content_type_'. $type_name] = array(
      '#type' => 'fieldset',
      '#title' => t('@type_name content type settings', array('@type_name' => ucfirst($type_name))),
      '#collapsible' => TRUE,
    );
    
    $options = _cckasetracker_fields_options(cckasetracker_get_content_fields($type_name, TRUE));
    $default_value = variable_get('cckasetracker_content_type_'. $type_name, array());
        
    $form['cckasetracker_content_type_'. $type_name]['cckasetracker_content_type_'. $type_name] = array(
      '#type' => 'checkboxes',
      '#options' => $options,
      '#default_value' => $default_value,
    );
  }

  // Pass the type name to the submit handler.
  $form['type_name'] = array(
    '#type' => 'hidden',
    '#value' => $type_name,
  );

  // Add submit handler.
  $form['#submit'][] = 'cckasetracker_admin_settings_submit';
        
  return system_settings_form($form);
}

/**
 * Submit handler; Set the content type variable according to selection.
 */
function cckasetracker_admin_settings_submit($form, $form_state) {
  if ($form_state['clicked_button']['#value'] == t('Save configuration')) {
    foreach ($form_state['values'] as $type_name => $fields) {
      if (strpos($type_name, 'cckasetracker_content_type_') === 0) {
        $type_name = str_replace('cckasetracker_content_type_', '', $type_name);        
        $fields = array_filter(drupal_map_assoc($fields));
        variable_set('cckasetracker_content_type_'. $type_name, $fields);
      }
    }
  }
}

/**
 * Get the options ready to be used in a form API select/ checkboxes.
 * @param $options
 * 
 * @param $selected
 *   If TRUE, return only fields that are cckase tracker fields.
 * @return
 *   Array of options or empty array.
 */
function _cckasetracker_fields_options($options, $selected = FALSE) {
  $return = array();
  foreach ($options as $value) {
    if (!(empty($value['cckasetracker']) && $selected)) {
      $return[$value['field_name']] = check_plain($value['label']) .' ('. check_plain($value['field_name']) .')';
    }
  }
  return $return;
}